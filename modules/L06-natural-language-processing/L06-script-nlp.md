---
layout: slides
title: L06--DSDH--Natural-Language-Processing
---

# 1.1 (1): L6: Natural Language Processing

Hobson Lane, UC San Diego
Instructor

<aside class="notes">
    I'm Hobson Lane.
    I am really excited about this week's lesson.
    Natural language processing is my favorite part of Data Science. 
    I love being able to put together machines that understand natural language and think for themselves.
    I enjoy it so much that four years ago I recruited some of my students to help me write a book about it.
    The publisher insisted we call it _Natural Language Processing in Action_.
    If we'd had our way we would have called how to build a chatbot to save you from information overload and manipulation. 
    Just as the flood of data has made machine learning possible, it is also overwhelming our brains.
    It's hard to tell rumor from fact.
    And everyone feels the need to be an expert at everything.
    In the healthcare field, there is even more pressure to understand it all.
    Natural Language Processing will help you in your healthcare work by relieving you of reading volumes of data on patients, drugs, and disease.
    Machines can now sift through all that for you.
    I truly believe that natural language processing could be the key to building prosocial machines that help save us from ourselves.
</aside>

# 1.2 (2): Logistics

<aside class="notes">
    Before we dive into natural language processing let's talk about logistics.
    It took me a bit longer to prepare the lesson this week.
    But I think you'll like it.
    I've broken the lengthy lectures into bite sized chunks.
    That way you can review the ones that are most useful, without searching through a long video.

    Also, I'm a big fan of the power of Open Source.
    So, I'll provide the slides in a separate ODP (Open Office Presentation) file.
    That way you can search the text and reuse any part of it in your office or at home.
    I'm releasing it under the Hippocratic "Do No Harm" (MIT) open source license.
    So feel free to reuse it however you like to help others get up to speed on Data Science for Healthcare.

    And if you know how to use `gitlab.com`, you can even what future lectures look like by visiting the gitlab link listed here.
</aside>

# 1.3 (3): Two Questions

What is Natural Language Processing?

How can NLP be used in Healthcare?

<aside class="notes">
    So you're probably wondering, "What is Natural Language Processing anyway?" and, "How can I use it in the healthcare profession?"
    That's what this lesson is all about. 
    You'll have the answers to these and many more questions by the end of this lesson.
</aside>

# 1.4 (4): Agenda

"each word’s meaning" -> `{  'each': 1, 'word': 2, "'s": 3, 'meaning': 4 }`
"What is Coronavirus?" -> `"COVID-19 is an infectious disease caused ..."`

Natural language
    Analyze text
    Represent text as numbers
    Meaningful representations
    Word vectors
Applications
    Semantic search
    Summarization
Conversational AI
    Therapy
    Triage

<aside class="notes">
    Here's your agenda for today.
    The goal of Natural Language Processing in to engineer some features from natural language text so that your normal Machine Learning Models can work with it.
    This means you need to convert natural language text into a numerical representation that captures its meaning.
    
    To do this you first must break the text into chunks that are meaningful.
    That way you can give each piece of text a numerical representation.
    So the first thing you'll learn is what those chunks of meaning are.
    Hint: these chunks of meaning are called *tokens*.
    
    After that you'll learn some meaningful numerical representations of those chunks as well as entire sentences, pages, or documents of text.
    One of the most popular and meaningful representations of tokens is called a word vector.
    You'll see some example uses for these word vectors and some of the challenges and the magic of these high dimensional word vectors.

    Then you will see how to use vector representations of natural language text for machine learning and semantic search.
    You may have heard of IBM Watson being used for medical diagnosis support and cognitive search.
    You will learn about this cognitive search application of natural language processing.

    Finally you will learn about conversational AI or chatbots and how they are used in healthcare.
    Conversational AI is particularly useful in underdeveloped healthcare systems such as in rural India, South America, Oceania, and Africa.
</aside>

# 1.5 (5): What is NLP

1. Psychology: human (animal) language understanding
2. Linguistics: natural language structure
3. Math: formal grammars for processing natural language
4. Engineering: computer systems that *understand* NL input and/or generate NL output
    - Understanding (NLU)
    - Generation (NLG)

<aside class=notes>
    If you study the psychological processes involved when you read or write or speak natural language, then your courses and graduate studies will likely be in the Psychology or Neuroscience department.

    If you study linguistics, you may have your own linguistics department and you will take courses in the computer science, English, and even social sciences departments.

    If you study the engineering of NLP systems most of your classes will be in a computer science department.
    As a computer scientist or software engineer you might think of your job as kind of like building compilers for natural language.

    If you study the grammars used for those compilers of natural language, your classes will be in the mathematics department.
    You might think of your role as designing formal logic grammars and using those to generate formal logical proofs of theorems, all in natural language that humans can understand.

    In this lecture you'll be shying away from the neuroscience and math behind natural language processing and focus more on the practicalities of it.
    You will concentrate most of your effort on the engineering of NLP systems that can feed your Data Science pipeline with data.

    Many scientists believe, like I do, that if you can build an NLP system, you will have a far deeper understanding of what's going on in your brain when you read text.
    This is the most practical and approachable aspect of NLP. 
    You will soon learn how to build a "recipe" of computer instructions (python code) to read, understand, and even write sensible natural language text.
    Hopefully I will be able to infect you with my fascination for the magic of machines that can interact with us in our own language.

    It's scary and awe inspiring at the same time, to think that you are interacting with those machines daily.
    And sometimes you may not even know that it's a machine and not a human on the other end of the conversation.
</aside>

[Foundational Issues in Natural Language Processing: Introduction](https://dash.harvard.edu/bitstream/handle/1/17017334/finlp.pdf), Sells, Peter, Stuart M. Shieber, and Thomas Wasow. 1991. (c) MIT Press.





----




# 2.1 (6): Tokens of Meaning

`tokenize("Each word's meaning is unique.")`

<aside>
    So now you're going to learn how to break text into pieces of meaning.
    In natural language processing you normally call these pieces of meaning tokens. 
    If you used a more specific term like characters or words you wouldn't have much flexibility.  
    The verb we use to talk about breaking text into tokens is *tokenize*.  
    The process of *tokening* text is call *tokenization*.

    It's probably not surprise that in most NLP packages, such as NLTK, the tokenizer can be found in an object called `Tokenizer`.  
    And the function you want to call is named `tokenize`. 
</aside>

# 2.2 (7): Text Pieces (tokens)

    tokenize("Each word ...")
Characters:

<aside>
    In some situations you will want to use characters as your atoms of meaning. 
    Characters are the smallest and simplest possible token for text processing.  
    
    Characters are a particularly useful token in healthcare, where new words and terminology are being created daily. 
    Think of the ever expanding list of drug names and chemical compound names and even surgical procedure names and codes.
    If you use characters as your atoms of text, then your NLP pipeline can understand brand new text, even if it's never seen a word before.

    How do you tokenize a string into characters in Python?
</aside>

# 2.3 (8): Text Pieces (tokens) 

    **list**("Each word ...")
Characters:

<aside>
    To separate a string into characters in Python you do not even need to install a tokenizer.  
    If you like you can use the built-in `list` function.  
    It will iterate through all the elements of an iterable and create a list of them all for you automatically.  
    Here is how you can do that for this natural language text string.
</aside>

# 2.4 (9): Text Pieces (tokens) 

    **list**("Each word ...")
Characters:
    `E, a, c, h, ...`

<aside>
    To separate a string into characters in Python you do not even need to install a tokenizer.  
    If you like you can use the built-in `list` function.  
    It will iterate through all the elements of an iterable and create a list of them all for you automatically.  
    Here is how you can do that for this natural language text string.
</aside>

# 2.5 (10): Text Pieces (tokens) 

    tokenize("Each word ...")
Words:

<aside>
    Characters work OK sometimes, but in many situations you will want to use words as your atoms of meaning. 
    Words are the way most human brains gather up concepts into one bucket and store those concepts. 
    You can think of characters as the bits or bytes in a computer. 
    They are very flexible and can be combined to represent almost anything. 
    But they don't really mean much in isolation. 
    You can't really get much information from single character in isolation.
    And there are infinite possibilities for all the character combinations you might see out there in the world.
    So it helps to narrow the focus of our machine, to only pay attention to the character combinations that appear together often.
    Words are those useful combinations. 
    And you can get a lot of meaning from a document just by knowing whether or not it contains a word.
    This is why you enter words into search engines like DuckDuckGo.  
    </aside>

# 2.6 (11): Text Pieces (tokens) 

    str.split("Each word ...")
Words:

<aside>
    Words are almost as easy to split in Python.  
    Python was made for processing text.  
    To separate a string into words in Python, you can get pretty good results just using the `split()` function.
    This will split your string into a list of strings based on whatever character you want to split on.  
    If you don't tell the split function which character you want to split on, it will just look for spaces between words.  
    This split function exists on the `str` type or object.  
    So if you like you can call the `str.split()` function to split a string into words.  
    </aside>

# 2.7 (12): Text Pieces (tokens) 

    "Each word ...".split()
Words:

<aside>
    Because `split` is a method on the `str` object, you can also call it as a method.  
    This might save you a few keystrokes, but it does exactly the same thing.  
    It will still return a list of all the space-delimited words in your string.    
    </aside>

# 2.8 (13): Text Pieces (tokens) 

    "Each word ...".split()
Words:
    ['Each', "word's", 'meaning', ... 'unique.'

<aside>
    If you use this quick and dirty tokenizer, `.split()`, you will get words that include punctuation.  
    If there's not space between characters then those characters get lumped together into a single token.  
    So contractions and hyphenated words are treated as a single packet of meaning.  
    This means your NLP pipeline will treat the word "unique" differently than "unique-period".
    Your NLP pipeline will be able to make better sense of the text if you separate the punctuation from words and split apart compound words and contractions.

    Imagine how important it is for duckduckgo to remove the period from the end of the word unique.  
    If it didn't do this, whenever your searches included the word unique, all those pages that had unique at the end of a sentence would be ignored.  
</aside>

# 2.9 (14): Text Pieces (tokens)

['Each', 'word', "'s", 'meaning', ...

<aside>
    Finally you get to see what a real tokenizer does and what tokens really look like.  
    Within the NLTK package there is a module called tokenize where you'll find a variety of tokenizers.  
    The one that you'll use for most healthcare problems is called `word_tokenize`.
    As you might guess it splits text into words.  
    It does it much better than the `.split()` method.  
    It pays attention to punctuation and splits contractions into two words or more.
</aside>

# 2.10 (15): Text Pieces (tokens)

['Each', 'word', "'s", 'meaning', ...

<aside>
    As you might guess the `word_tokenize` function splits text into real words.  
    It does it much better than the `.split()` method.  
    It pays attention to punctuation and splits contractions into two words or more.
    So you won't end up with periods and question marks clinging to your words.  
</aside>




----

# N-Grams

# 3.1 (16): Character-level N-grams

1-gram: h

<aside>
    not much meaning to an isolated character level 1-gram
    counting up the frequency of the character 1-gram "h" might not help you much, unless you were trying to decrypt a simple child's game cypher like a Roman Cypher or a secret message encrypted by a child with a decoder ring from a box of captain crunch or cracker jacks.
</aside>

# 3.2 (17): Character-level N-grams

2-gram: he

<aside>
    male human in your documents
</aside>

# 3.3 (18): Character-level N-grams

3-gram: hea

<aside>
    not in your dictionary
</aside>

# 3.4 (19): Character-level N-grams

4-gram: hear

<aside>
    audiology lab?
</aside>

# 3.5 (20): Character-level N-grams

5-gram: heart

# 3.6 (21): Character-level N-grams

tokenize("Each word's …", n_grams=(1,3))

<aside>
    Just when you thought you knew all the different ways to tokenize text, there's more!
    N-grams.
    Gram is just another word for token.  
    A gram can be a character a word-piece or a whole word.  

    An N-gram is a continuous sequence of `N` tokens or grams.
    It turns out that tokens are more meaningful if you consider pairs and triplets of them together. 
    You can use the n_grams argument in most tokenizers to specify a range of N values you'd like to consider.  
    For these examples you'll be working with 1-grams, 2-grams, and 3-grams.  
    So we use the n_grams argument of (1, 3), specifying the minimum and maximum number of n_grams you want.  
</aside>

# 3.7 (22): Character-level N-grams

tokenize("Each word's …", n_grams=(1,3))

# 3.8 (23): Character-level N-grams

tokenize("Each word's …", n_grams=(1,3))

# 3.9 (24): Character-level N-grams

"**E**ach word's meaning is unique"

# 3.10 (25): Character-level N-grams

"E**a**ch word's meaning is unique"

# 3.11 (26): Character-level N-grams

"Ea**c**h word's meaning is unique"

# 3.12 (27): Character-level N-grams

"Eac**h** word's meaning is unique"

# 3.13 (28): Character-level N-grams

"Each word's meaning is unique"

" "

# 3.14 (29): Character-level N-grams

"Each word's meaning is unique"

"w"

# 3.15 (30): Character-level N-grams

"**Ea**ch word's meaning is unique"

'E', 'a'
"Ea"

# 3.16 (31): Character-level N-grams

"Each word's meaning is unique"

'E', 'a', 'c'
'Ea', 'ac'
'Eac'

# 3.17 (32): Character-level N-grams

"Each word's meaning is unique"

'h', ' ', 'w'
'h ', ' w'
'h w'

# 3.18 (33): Character-level N-grams

The quick brown fox jumped over the lazy dog.
len(Counter("The quick ..."))

# 3.18 (33): Character-level N-grams

The quick brown fox jumped over the lazy dog.
len(Counter("The quick ..."))

<aside>
    How many unique characters do you expect to find in the text for this sentence. 
    It should contain all the characters in the English language, right?
    Will this python code return 23, one count for each letter om Emglish?
</aside>

# 3.19 (34) Character-level Frequency Analysis

>>> len(Counter("The quick brown fox jumped over the lazy dog."))
28

<aside>
    Why 28? Where did the extra letters come from.
</aside>

# 3.20 (35)  Character-level Frequency Analysis

from collections import Counter
Counter("The quick brown fox jumped over the lazy dog.")

{' ': 8, 'e': 4, 'o': 4, 'h': 2, 'u': 2,
 'r': 2, 'd': 2, 'T': 1, 'q': 1, 'i': 1,
 'c': 1, 'k': 1, 'b': 1, 'w': 1, 'n': 1,
 'f': 1, 'x': 1, 'j': 1, 'm': 1, 'p': 1,
 'v': 1, 't': 1, 'l': 1, 'a': 1, 'z': 1,
 'y': 1, 'g': 1, '.': 1}

# 3.21 (36) Capitalization Matters

from collections import Counter
Counter("The quick brown fox jumped over the lazy dog.")

{' ': 8, 'e': 4, 'o': 4, 'h': 2, 'u': 2,
 'r': 2, 'd': 2, **'T': 1**, 'q': 1, 'i': 1,
 'c': 1, 'k': 1, 'b': 1, 'w': 1, 'n': 1,
 'f': 1, 'x': 1, 'j': 1, 'm': 1, 'p': 1,
 'v': 1, 't': 1, 'l': 1, 'a': 1, 'z': 1,
 'y': 1, 'g': 1, '.': 1}

 # 3.22 (37) Don't forget white space

from collections import Counter
Counter("The quick brown fox jumped over the lazy dog.")

{**' ': 8**, 'e': 4, 'o': 4, 'h': 2, 'u': 2,
 'r': 2, 'd': 2, 'T': 1, 'q': 1, 'i': 1,
 'c': 1, 'k': 1, 'b': 1, 'w': 1, 'n': 1,
 'f': 1, 'x': 1, 'j': 1, 'm': 1, 'p': 1,
 'v': 1, 't': 1, 'l': 1, 'a': 1, 'z': 1,
 'y': 1, 'g': 1, '.': 1}

# 38

Word level N-grams

# 39

Word-lvel N-gram examples

cream
John Lewis
Flowers for Algernon
Rosa ... Parks
21 Lessons ...

# 40: Applications

- Search:
  - Information Retrieval
  - Information Extraction
  - Question Answering
- Summarization
- Triage

# Search

TFIDF Vectors for search

<aside>
    If a page contains the words you're looking for, 
    Thinking about words a bit more for a moment, consider the ambiguity 
    Think of all the concepts gathered up in the word "heart".
    You can think of it in a medical context as that piece of muscle that pumps blood.
    You might also talk to patients about having heart or courage to get through a tough time, like this global pandemic.
    Or you might even
</aside>

# Application: Translation

English -> French

# Application: English -> French

Google Translate

<aside>
    Most natural language processing technology made advances by learning to translate texts.
    There are a massive amount of training examples where books and documents have been translated by humans.
    Machines learned from our translation work.
    </aside>

# Application: Translation

English -> French
Latin -> English

# Application: Translation

Medical Jargon -> Common English
Drug Names -> Chemical Compound Names

<aside>
    Style transfer works for technical jargon as well as playful examples you might see online: making your text look like Hemmingway, or Shakespear.
    Useful in building apps and interfaces to medical data, including a patient's medical record, understandable by regular humans.
    <aside>

"Los computadores comprende Francaise ausi" -> french2english NLP -> "Computers understand French as well."

<aside class="notes">
building machines that could understand french text and generate equivalent English statements is one of the most important problems in NLP that brought
</aside>

# Sentiment Analysis

"Google stinks!" # use vader and get 2-d score
"What is NLP" # use word2vec and get 4x3 score





# ONC

Office of the National Coordinator for Health IT (ONC)

### Resources

[Open Standards for Certifying EHR Systems](https://www.healthit.gov/buzz-blog/electronic-health-and-medical-records/international-health-standards-closer-onc-efforts)
ucsd-digital-health-data-science-L8-nlp-slides

# 1: What is NLP

1. Psychology: human (animal) language understanding
2. Linguistics: natural language structure
3. Math: formal grammars for processing natural language
4. Engineering: computer systems that *understand* NL input and/or generate NL output
    - Understanding (NLU)
    - Generation (NLG)

::: notes

If you study the psychological processes involved when you read or write or speak natural language, then your courses and graduate studies will likely be in the Psychology or Neuroscience department.

If you study linguistics, most of your courses will be in the English department, or whatever your native language is.

If you study the engineering of NLP systems most of your classes will be in a computer science department and you will think of your job as kind of like building compilers for natural language.

If you study the grammars that are useful of NLP your classes will be in the mathematics department and you will think of your job as designing formal logic grammars and using those to generate formal logical proofs of theorems in natural language.

You will learn about all of these perspectives on NLP in this lecture.
But you will concentrate most of your effort on the engineering of NLP systems.
In my opinion, if you can build an NLP system, you will have a far deeper understanding of the psychology and
This is the most practical and aproachable aspect of NLP. You will soon learn how to build a "recipe" of computer instructions (python code) to read, understand, and generate natural language text.
Hopefully I will be able to infect you with my fascination for the magic of machines that can interact with us in our own language so well that we can hardly tell they are not human.
It's scary and inspiring at the same time.

:::

[Foundational Issues in Natural Language Processing: Introduction](https://dash.harvard.edu/bitstream/handle/1/17017334/finlp.pdf), Sells, Peter, Stuart M. Shieber, and Thomas Wasow. 1991. (c) MIT Press.

# 2: Magical Valley of NLP

![Mashiro Mori's Uncanny Valley diagram. This shows user satisfaction with humanoid robotics technology on teh vertical axis. The horizontal axis shows the advancement of technology and time. More advanced technologies are shown on the right of the plot. As technology advances from left to right there is a gradual increase in satisfaction with technology until it reaches the lip of the uncanny valley and user satisfaction drops while anxiety increases, due to the creepiness of some advanced robotic systems. to the right of this dip, customer satisfaction climbs again as technology advances and we forget that the machine is not a human.](media/uncanny_valley.png)

::: notes

Diagram alt text: ...

Let's explore deep into the uncanny valley together.

The diagram here was sketched by Mashiro Mori in 1970, just as personal computers were being developed and systems like conversational AI programs like ELIZA were becoming popular.

Systems that look like a human body, but don't act like them probably trigger an emotional response similar to what you experience when you see a dead body (corpse) or see a zombie movie.
We have probably evolved to detect the subtle behavioral cues of sickness or mental illness.
This same discomfort with humanoid robots can also sometimes be triggered by chatbots, estpecially if the bot is given a voice that sounds relatively human.
See if you can sense your own creepiness as you have a conversation with Alexa or Google Assistant.
Those engineers have worked very hard to ensure that you don't get creeped out.
This isn't the creepiness you feel when it seems like the bot anticipates your actions before you even know you are going to do something.
This is the creepiness of hearing some subtle cues in the voice that is unnatural, while at the same time imagining that it is a human mind at work.


:::

[Uncanny Valley](https://en.wikipedia.org/wiki/Uncanny_valley#/media/File:Mori_Uncanny_Valley.svg)
[Mashiro Mori](https://en.wikipedia.org/wiki/Masahiro_Mori_(roboticist))

# 4: Language Model

Your blood pressure is too ______ .

How old are ________ ?

Does it hurt when you ________ ?

Do you have a family history of ________ ?

Dr. ________ told me the diagnosis .

Patient LDL level is 100, ________ level is 50, and the total is            .

Dr. Smith gave me ________ best estimate .


# 5: Your blood pressure is too

P=0.65   high

P=0.30   low

P=0.05   erratic


::: notes

Your
:::

# 6: How old are

P=0.95   you

P=0.03   they

P=0.01   y'all

P=0.01   yous

# 7: Does it hurt

How old are you? P=0.95

How old are they? P=0.03

How old are y'all? P=0.01

How old are yous? P=0.01


Do you have a family history of ________ ?

Dr. ________ told me the diagnosis .

Patient LDL level is 100, ________ level is 50, and the total is            .

Dr. Smith gave me ________ best estimate .


# 3: Monty Hall Fallacy

Real life applications:

- you suspect a patient has one of 3 genes that could cause a disease and you plan to treat based on that one genetic abnormality, then another doctor does a test that confirms the patient does not have one of the others. Should you change your treatment? (must assume that only 1 Gene is present otherwise the abnormality would be greater and the cause obvious, like XOR condition).
- collider on the door chosen to be opened by Monty Hall

# 4: Explain Away Fallacy

- collider where one cause negates the other
- two possible causes of test results then find out that one cause is present, that makes it less likely that the other cause is present

# 5: Berkson's Paradox

- 2 diseases required for admission to hospital (admission's bias) causing `bone disease` and `resperatory disease` to be correlated

                   , general populaiton, general populaiton, general populaiton, hospitalized
resperatory disease, bone disease yes,    bone disease no,   % bone disease,


# 6: Before Cause there was Corroboration

- strength of correlation
- agreement with physics
-

